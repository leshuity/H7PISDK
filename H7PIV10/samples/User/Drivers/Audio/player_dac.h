#ifndef __PLAYER_DAC_H__
#define __PLAYER_DAC_H__

#ifdef __cplusplus
extern "C" {
#endif

#include "stm32h7xx_hal.h"
#include "player_dev.h"


/*****************************************************
 * 
 * STM32 API and definition
 * 
 ****************************************************/
//
//os definition if used os
#include "FreeRTOS.h"
#include "task.h"
#include "cmsis_os.h"
#define PLAYER_DELAY_MS         (void*)osDelay
//
//dac definition
extern  DAC_HandleTypeDef       hdac1;
#define PLAYER_DAC_HANDLER      &hdac1
#define PLAYER_CH_LEFT          DAC_CHANNEL_2
//
//timer definition
extern  TIM_HandleTypeDef       htim6;
#define PLAYER_TIMER_FREQ       240000000
#define PLAYER_TIMER_PRESCALER  1
#define PLAYER_TIMER_HANDLE     &htim6
//
//shut down pin
#define PLAYER_SD_PIN           GPIOA,GPIO_PIN_3



/*****************************************************
 * 
 * dac definition
 * 
 ****************************************************/
typedef struct
{
	//buffer
	uint8_t    buffer0[PLAYER_BUFFER_SIZE];
	uint8_t    buffer1[PLAYER_BUFFER_SIZE];
	//buffer pointer
	uint8_t*  (buffer_switch[2]);
	//buffer calid status
	uint8_t   buffer_valid[2];
	//bufer 0 or 1 index
	uint8_t   buffer_index;
	//data smaplerate
	uint32_t  samplerate;
	player_dev_status_t status;

}player_dac_dev_t;
/*****************************************************
 * 
 * player dac device init
 * 
 ****************************************************/
void player_dev_dac_init(player_dev_t* dev);
void player_dac_irq_conv_complete(void);

#ifdef __cplusplus
}
#endif

#endif

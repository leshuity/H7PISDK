#ifndef __IRQ_H__
#define __IRQ_H__

#include "stm32h7xx_hal.h"
#include "sys.h"



/***************************************
* 
* spi2 irq
*
***************************************/
extern uint8_t spi2_txend;
extern uint8_t spi2_rxend;


#endif

#include "app_config.h"

#ifdef app_testmotor

osThreadId app_handler_testmotor;

/*********************************************************************


**********************************************************************/
void app_testmotor_task(void const * argument)
{
	rz_Init();
	rz_Start();
	while(1)
	{
		
		rz_SetDirection(RZ_DIR_FORWARD,10);
		osDelay(1000);
		rz_SetDirection(RZ_DIR_FORWARD,50);
		osDelay(1000);
		rz_SetDirection(RZ_DIR_FORWARD,90);
		osDelay(1000);
	}
}

/*********************************************************************


**********************************************************************/
void app_testmotor_start(void)
{	

  osThreadDef(testmotor_task, app_testmotor_task, osPriorityNormal, 1, 1024);
	app_handler_testmotor = osThreadCreate(osThread(testmotor_task), NULL);
}

#endif